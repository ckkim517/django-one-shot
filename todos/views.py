from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoListForm, UpdateForm, CreateItemForm, EditItemForm


# Create your views here.


# create a view that will get all of the instances of TodoList model
# put them in the context for the template
def todo_lists(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_lists": todo_lists,
    }
    return render(request, "todos/list.html", context)


# create a view that shows the details of a particular to-do list
# including its tasks
def todo_list_detail(request, id):
    todo_list_detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": todo_list_detail,
    }
    return render(request, "todos/detail.html", context)


# create a view for the TodoList model that will show the name field
def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save(False)
            todo_list.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create_todolist.html", context)


# create an update view for the TodoList model that will show the name field
def todo_list_update(request, id):
    post = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = UpdateForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save()
            return redirect("todo_list_detail", id=post.id)
    else:
        form = UpdateForm(instance=post)
    context = {
        "form": form,
    }
    return render(request, "todos/update_todolist.html", context)


# create a delete view for the TodoList model
def todo_list_delete(request, id):
    post = TodoList.objects.get(id=id)
    if request.method == "POST":
        post.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


# create a view for the TodoItem model that will show the task field
def todo_item_create(request):
    if request.method == "POST":
        form = CreateItemForm(request.POST)
        if form.is_valid():
            todo_list = form.save(False)
            todo_list.save()
            return redirect("todo_list_detail", id=todo_list.list.id)
    else:
        form = CreateItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create_todoitem.html", context)


# create an update view for the TodoItem model that will show the model fields
def todo_item_update(request, id):
    post = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = EditItemForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save()
            return redirect("todo_list_detail", id=post.id)
    else:
        form = EditItemForm(instance=post)
    context = {
        "form": form,
    }
    return render(request, "todos/update_todoitem.html", context)
